#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Milan Ondrasovic <milan.ondrasovic@gmail.com>

import math
import autograd

import cv2 as cv
import numpy as np
# import autograd.numpy as np

from typing import *

from scipy import optimize


class AugmentedHomographyModel:
    def __init__(self, src_points_groups: np.ndarray, dst_points_groups: np.ndarray) -> None:
        """Builds an objective function for the  perspective transformer using the presence of
        multiple groups of source points to refine the homography transformation using nonlinear
        optimization.

        :param src_points_groups: a list of groups of source points belonging to different objects
        in the plane while sharing the same dimensional properties
        :param dst_points_groups: a list of groups of destination points to which all the source
        point groups have to be mapped in the same fashion. However, only the first group will be
        used as the main one, therefore if all the groups contain the same points nothing
        unexpected will happen.
        """
        assert src_points_groups.ndim == 3 and src_points_groups.shape == dst_points_groups.shape
        
        # Main points are given by the first group.
        self._main_points: np.ndarray = src_points_groups[0]
        self._aux_points_groups: np.ndarray = src_points_groups[1:]
        self._dst_points: np.ndarray = dst_points_groups[0]
        
        # Number of values that actually contribute to the loss function. In this setting, we also
        # consider those "ones" representing homogeneous coordinates.
        self._vals_num: float = float(len(src_points_groups.flatten()))
        
        # A vector to be populated by 8 coefficients of the homography matrix and later on reshaped
        # into a 3x3 matrix. This is to save computational time by not having to create new matrix
        # for every objective function call.
        self._homography_template: np.ndarray = np.ones(9, np.float32)
        
        # A matrix to be populated in each iteration by rotation and translation coefficients. This
        # is to save computational time by not having to create new matrix for every objective
        # function call and then also for every group of auxiliary points .
        self._affine_matrix_template: np.ndarray = np.eye(3, dtype=np.float32)

    @staticmethod
    def preprocess_points_homo_matrix(points: np.ndarray) -> np.ndarray:
        # Returns a matrix containing points in homogeneous coordinates stored as column vectors.
        assert points.ndim == 2
        return np.concatenate((np.float32(points).T, np.ones((1, len(points)))))
    
    def build_homography(self, x: np.ndarray) -> np.ndarray:
        assert x.ndim == 1 and len(x) >= 8

        return np.append(x[:8], 1).reshape(3, 3)
        # self._homography_template[:8] = x[:8]
        #
        # return self._homography_template.reshape(3, 3)
    
    def _iterate_affine_transform_matrices(self, x: np.ndarray) -> Iterator[np.ndarray]:
        assert x.ndim == 1 and len(x) >= 8 and (len(x) - 8) % 3 == 0
        
        for i in range(8, len(x), 3):
            theta, transl_x, transl_y = x[i:i + 3]
            sin_theta, cos_theta = np.sin(theta), np.cos(theta)

            self._affine_matrix_template[0, 0] = cos_theta
            self._affine_matrix_template[0, 1] = -sin_theta
            self._affine_matrix_template[0, 2] = transl_x

            self._affine_matrix_template[1, 0] = sin_theta
            self._affine_matrix_template[1, 1] = cos_theta
            self._affine_matrix_template[1, 2] = transl_y
            
            yield self._affine_matrix_template
    
    def __call__(self, *args, **kwargs) -> float:
        x = args[0]
        assert (x.ndim == 1) and (len(x) >= 8)
        
        homography = self.build_homography(x)
        main_points_persp_transformed = np.matmul(homography, self._main_points)
        
        # Broadcast the divison by the z coordinate and convert each column vector of
        # [x, y, z] to [x', y', 1].
        main_points_homo = main_points_persp_transformed / main_points_persp_transformed[-1, :]
        
        # Compute the loss function for the main points.
        loss = np.sum(np.power(main_points_homo - self._dst_points, 2))
        
        for aux_points, affine_matrix in zip(
                self._aux_points_groups,
                self._iterate_affine_transform_matrices(x)):
            aux_points_persp_transformed = np.matmul(homography, aux_points)
            aux_points_affine_transformed = np.matmul(
                affine_matrix,
                aux_points_persp_transformed)
            aux_points_homo = aux_points_affine_transformed / aux_points_affine_transformed[-1, :]

            # Compute the loss function for the auxiliary points.
            loss += np.sum(np.power(aux_points_homo - self._dst_points, 2))
        
        # Compute the average loss (all the terms have already been summed up).
        return loss / self._vals_num


# Callback parameters: epoch index and loss function value. Return false if the training should
# stop, otherwise return true.
EpochCbT = Callable[[int, float], bool]


class AugmentedHomographyTrainer:
    def __init__(self, src_points_groups: np.ndarray, dst_points_groups: np.ndarray) -> None:
        """Builds a perspective transformer trainer using the presence of multiple groups of source
        points to refine the homography transformation using nonlinear optimization.

        :param src_points_groups: a list of groups of source points belonging to different objects
        in the plane while sharing the same dimensional properties
        :param dst_points_groups: a list of groups of destination points to which all the source
        point groups have to be mapped in the same fashion. However, only the first group will be
        used as the main one, therefore if all the groups contain the same points nothing
        unexpected will happen.
        """
        assert src_points_groups.ndim == 3 and src_points_groups.shape == dst_points_groups.shape

        self._src_points_groups: np.ndarray = src_points_groups
        self._dst_points_groups: np.ndarray = dst_points_groups
        
        self._src_points_groups_homo: Optional[np.ndarray] = None
        self._dst_points_groups_homo: Optional[np.ndarray] = None

    def train(self, epoch_cb: Optional[EpochCbT] = None) -> Tuple[np.ndarray, np.ndarray]:
        """Trains the augmented homography model. Returns the history of loss function for each
        epoch.

        :param epoch_cb: an optional callback to be called after each epoch. If the function returns
        false the training stops, otherwise it should return true.
        :return: the best homography matrix that could be found together with the history of the
        loss function evaluation after each epoch
        """
        best_main_points_idx, homography_seed, aux_coefs_seed = self._find_best_main_and_aux_seeds()
        
        def rearrage_and_preprocess_groups(points_groups):
            # Move the best candidate for the main points group to the beginning of the list.
            points_groups_rearranged = np.copy(points_groups)
            points_groups_rearranged[0, :] = points_groups[best_main_points_idx, :]
            points_groups_rearranged[best_main_points_idx, :] = points_groups[0, :]

            # Merged all the points groups into a 3D tensor, i.e. a list of matrices.
            # Each matrix has 3 rows because points are converted to homogeneous coordinates.
            # The number of columns is equal to the number of points.
            shape = (len(points_groups_rearranged), 3, len(points_groups_rearranged[0]))
            points_homo_tensor = np.empty(shape, dtype=np.float32)
            preprocess_points = AugmentedHomographyModel.preprocess_points_homo_matrix

            for i, points_group in enumerate(points_groups_rearranged):
                points_homo_tensor[i, :] = preprocess_points(points_group)
            
            return points_homo_tensor
        
        self._src_points_groups_homo = rearrage_and_preprocess_groups(self._src_points_groups)
        self._dst_points_groups_homo = rearrage_and_preprocess_groups(self._dst_points_groups)
        
        return self._find_best_homography(homography_seed, aux_coefs_seed, epoch_cb)

    def _find_best_main_and_aux_seeds(self) -> Tuple[int, np.ndarray, Optional[np.ndarray]]:
        best_combo_loss = float('inf')
        best_main_points_idx = best_homography = best_affine_params = None

        for i in range(len(self._src_points_groups)):
            combo_loss, homography, affine_params = self._eval_main_aux_points_combo(i)

            if combo_loss < best_combo_loss:
                best_combo_loss = combo_loss
                best_main_points_idx = i
                best_homography, best_affine_params = homography, affine_params

        return best_main_points_idx, best_homography.flatten()[:8], best_affine_params

    def _eval_main_aux_points_combo(
            self,
            main_points_idx: int) -> Tuple[float, np.ndarray, Optional[np.ndarray]]:
        # Find the homography with respect to the main points group.
        main_points = self._src_points_groups[main_points_idx]
        dst_points = self._dst_points_groups[main_points_idx]
        
        homography = self._find_single_homography(main_points, dst_points)
        main_points_t = self._transform_points(main_points, homography)
        main_centroid = np.mean(main_points_t, axis=0)
        
        affine_params = []
        total_alignment_loss = 0

        for i, aux_points in enumerate(self._src_points_groups):
            if i == main_points_idx:
                continue

            # Transform an auxiliary points groups using the homography computed with respect to the
            # main group.
            aux_points_t = self._transform_points(aux_points, homography)
            aux_centroid = np.mean(aux_points_t, axis=0)

            # Compute the translation and rotation estimates. Most of the time they are way off.
            translation = main_centroid - aux_centroid
            affine_transform_estim = cv.estimateAffinePartial2D(aux_points_t, main_points_t)[0]
            angle = -math.atan2(affine_transform_estim[0, 1], affine_transform_estim[0, 0])

            transform_mat = self._affine_transform_mat(angle, *translation)

            # Try to approximately match the affine transformation of the auxiliary points group to
            # the main points group given the perspective deformation is identical.
            aux_points_homo = cv.convertPointsToHomogeneous(aux_points_t)
            aux_points_matched_homo = cv.transform(aux_points_homo, transform_mat)
            aux_points_matched = np.squeeze(
                cv.convertPointsFromHomogeneous(aux_points_matched_homo),
                1)
            
            # TODO Consider using MSE here also, because the objective function uses it. Maybe it
            #      would be better to search the initial state with respect to the same criterion.
            
            # Compute the sum of absolute errors of the deviation. How well does this affine
            # transformation align all the points groups after rectification?
            total_alignment_loss += np.abs(main_points_t - aux_points_matched).sum()

            # Save the affine transformation parameters.
            affine_params.extend((angle, *translation))

        return (total_alignment_loss,
                homography,
                None if len(affine_params) == 0 else np.float32(affine_params))

    @staticmethod
    def _find_single_homography(src_points: np.ndarray, dst_points: np.ndarray) -> np.ndarray:
        return cv.findHomography(src_points, dst_points)[0]

    @staticmethod
    def _transform_points(points: np.ndarray, homography: np.ndarray) -> np.ndarray:
        return np.squeeze(cv.perspectiveTransform(points.reshape(-1, 1, 2), homography), 1)

    @staticmethod
    def _affine_transform_mat(theta: float, transl_x: float, transl_y: float) -> np.ndarray:
        sin_theta, cos_theta = np.sin(theta), np.cos(theta)
        return np.float32((
            (cos_theta, -sin_theta, transl_x),
            (sin_theta, cos_theta, transl_y),
            (0, 0, 1)
        ))

    def _find_best_homography(
            self,
            homography_seed: np.ndarray,
            aux_coefs_seed: Optional[np.ndarray] = None,
            epoch_cb: Optional[EpochCbT] = None) -> Tuple[np.ndarray, np.ndarray]:
        model = AugmentedHomographyModel(self._src_points_groups_homo, self._dst_points_groups_homo)
        
        x_0 = homography_seed
        if aux_coefs_seed is not None:
            x_0 = np.concatenate((x_0, aux_coefs_seed))
        
        # x_0 += np.random.rand(len(x_0)) * 1e-6
        
        # optim_params = dict(
        #     fun=model,
        #     x0=x_0,
        #     method='BFGS',
        #     # jac=autograd.jacobian(model),
        #     options=dict(
        #         disp=True,
        #         maxiter=1000,
        #     ))

        minimizer_kwargs = dict(
            method='BFGS',
            # jac=autograd.jacobian(model),
            options=dict(
                disp=False,
                maxiter=1000,
            ))
        
        iters_num = 100
        result = optimize.basinhopping(
            func=model,
            x0=x_0,
            niter=iters_num,
            disp=True,
            minimizer_kwargs=minimizer_kwargs)


        # result = optimize.minimize(**minimizer_kwargs)
        loss_history = [result.fun]
        best_homography = model.build_homography(result.x)
        
        print('*' * 70)
        print(f'results:\n{result}')

        return best_homography, np.float32(loss_history)
