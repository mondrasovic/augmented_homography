import cv2 as cv
import numpy as np

from scipy.spatial.transform import Rotation


# points_2d = cv.projectPoints(
#     points_3d.reshape(-1, 3),
#     rvec,
#     tvec,
#     camera_matrix,
#     None)[0]
# points_2d = np.squeeze(points_2d, 1).reshape(*points_3d.shape[:2], 2).round().astype(np.int)
# points_2d.shape
#
# min_x, max_x = np.min(points_2d[..., 0]), np.max(points_2d[..., 0])
# min_y, max_y = np.min(points_2d[..., 1]), np.max(points_2d[..., 1])
#
# print(f'x: {min_x} --> {max_x}')
# print(f'y: {min_y} --> {max_y}')
#
# new_height, new_width = max_y - min_y, max_x - min_x

# image_3d = np.zeros((new_height, new_width))
# cv.polylines(image_3d, points_2d, True, 255, 1, cv.LINE_AA)
#
# image_3d = cv.resize(image_3d, None, fx=0.5, fy=0.5)
# image_show(image_3d)

# %%

def draw_axes(image, rvec, tvec, camera_matrix, axis_length=100):
    points = np.float32(
        ((axis_length, 0, 0),
         (0, axis_length, 0),
         (0, 0, axis_length),
         (0, 0, 0))).reshape(-1, 3)
    axis_points = cv.projectPoints(points, rvec, tvec, camera_matrix, (0, 0, 0, 0))[0]
    
    def _draw_line(end_point, color):
        cv.line(image, tuple(axis_points[3].ravel()), end_point, color, 5, cv.LINE_AA)
    
    _draw_line(tuple(axis_points[0].ravel()), (255, 0, 0))
    _draw_line(tuple(axis_points[1].ravel()), (0, 255, 0))
    _draw_line(tuple(axis_points[2].ravel()), (0, 0, 255))


if __name__ == '__main__':
    import math
    import functools
    
    
    window_name = '3D rotation'
    
    image_shape = (1000, 1200, 3)
    translation_range = 500
    angle_range = 180
    axis_translation = [0, 0, 0]
    axis_rotation_angles = [0, 0, 0]
    
    
    def surface_height_func(x_0, x_1, h):
        p = (x_0 + x_1) / 2
        s = -(4 * h) / ((x_0 + x_1) ** 2)
        
        def _f(x):
            return (s * (x - p) ** 2) + h
        
        return _f
    
    
    max_dim = np.max(image_shape)
    surface_height = max_dim * 0.7
    height_func = surface_height_func(0, image_shape[1], surface_height)
    X, Y = np.meshgrid(np.arange(0, image_shape[1]), np.arange(0, image_shape[0]))
    Z = height_func(X) + 10
    
    
    def update_image():
        rot_mat = Rotation.from_euler('xyz', axis_rotation_angles, degrees=True).as_dcm()
        rvec = cv.Rodrigues(rot_mat)[0]
        tvec = np.float32(axis_translation)
        fx = fy = 1.0
        cx, cy = image_shape[1] / 2.0, image_shape[0] / 2.0
        camera_matrix = np.array((
            (fx, 0, cx),
            (0, fy, cy),
            (0, 0, 1)))
        image = np.zeros(image_shape)
        cv.drawFrameAxes(image, camera_matrix, None, rvec, tvec, 100, 5)
        # draw_axes(image, rvec, tvec, camera_matrix)
        cv.imshow(window_name, image)
    
    
    def on_axis_rotation_angle_trackbar_change(axis_index, angle_degrees):
        global axis_rotation_angles
        axis_rotation_angles[axis_index] = math.radians(angle_degrees)
        update_image()
    
    
    def on_axis_translation_trackbar_change(axis_index, value):
        global axis_translation
        axis_translation[axis_index] = value
        update_image()
    
    
    cv.namedWindow(window_name)
    
    for i, label in enumerate(('Theta (x)', 'Phi (y)', 'Gamma (z)')):
        cv.createTrackbar(
            label,
            window_name,
            0,
            angle_range,
            functools.partial(on_axis_rotation_angle_trackbar_change, i))
    
    # for i, label in enumerate(('Translation (x)', 'Translation (y)', 'Translation (z)')):
    #     cv.createTrackbar(
    #         label,
    #         window_name,
    #         0,
    #         translation_range,
    #         functools.partial(on_axis_translation_trackbar_change, i))
    
    update_image()
    
    cv.waitKey(0)
    cv.destroyAllWindows()
