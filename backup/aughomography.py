#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Milan Ondrasovic <milan.ondrasovic@gmail.com>

import logging
import time
import itertools
import dataclasses

from typing import Callable, Optional, List

import cv2 as cv
import numpy as np
import torch


_log = logging.getLogger(__name__)

# Callback parameters: epoch index and loss function value. Return false if the training should
# stop, otherwise return true.
EpochCbT = Callable[[int, float], bool]
TensorT = torch.tensor


class TransformationNotFoundError(Exception):
    pass


@dataclasses.dataclass(frozen=True)
class OptimizationResult:
    used_points_groups_indices: np.ndarray
    homography: np.ndarray
    loss_history: np.ndarray
    elapsed_time: float


@dataclasses.dataclass(frozen=True)
class _OptimizationConfig:
    learning_rate: float = 0.0001
    max_global_iter: int = 10 # 8
    max_local_iter: int = 300 # 100
    loss_eps: int = 1e-6
    determinant_weight: float = 0.8
    max_time_sec: int = 300 # 60
    optimizer_max_iter: int = 40
    tolerance_grad: float = 1e-7
    history_size: int = 200
    epoch_cb: Optional[EpochCbT] = None


@dataclasses.dataclass(frozen=True)
class _PointsGroupEvalStats:
    ref_points_group_index: int
    loss: float
    homography: TensorT
    homography_det: float


def l2_norm_loss(rectified_points: TensorT, target_points: TensorT) -> TensorT:
    return torch.mean(torch.norm(rectified_points - target_points))


def se_determinant_loss(matrix: TensorT, target_determinant: float) -> TensorT:
    return ((matrix[0, 0] * (matrix[1, 1] * matrix[2, 2] - matrix[2, 1] * matrix[1, 2]) -
             matrix[0, 1] * (matrix[1, 0] * matrix[2, 2] - matrix[2, 0] * matrix[1, 2]) +
             matrix[0, 2] * (matrix[1, 0] * matrix[2, 1] - matrix[2, 0] * matrix[1, 1])) -
            target_determinant) ** 2


def l2_norm_se_determinant_loss(
        rectified_points: TensorT,
        target_points: TensorT,
        homography: TensorT,
        target_determinant: float = 1.0,
        determinant_weight: float = 0.5) -> TensorT:
    return (1 - determinant_weight) * l2_norm_loss(rectified_points, target_points) + \
           determinant_weight * se_determinant_loss(homography, target_determinant)


def translation_loss(translation_coefs: TensorT) -> TensorT:
    return torch.sum(torch.abs(translation_coefs))

def experimental_loss(
        rectified_points: TensorT,
        target_points: TensorT,
        homography: TensorT,
        translation_coefs: TensorT,
        target_determinant: float = 1.0,
        determinant_weight: float = 0.5) -> TensorT:
    return (1 - determinant_weight) * l2_norm_loss(rectified_points, target_points) + \
           determinant_weight * se_determinant_loss(homography, target_determinant)

def _transform_points(points: np.ndarray, homography: np.ndarray) -> np.ndarray:
    return np.squeeze(cv.perspectiveTransform(points.reshape(-1, 1, 2), homography), 1)


def _T(points: np.ndarray) -> TensorT:
    return torch.from_numpy(points).float()


def _convert_to_homogeneous(points: np.ndarray) -> TensorT:
    # Each matrix has 3 rows because points are converted to homogeneous coordinates.
    # The number of columns is equal to the number of points.
    dummy_ones = np.ones((*points.shape[:-1], 1))
    homogeneous_points = np.transpose(np.dstack((points, dummy_ones)), axes=(0, 2, 1))
    return _T(homogeneous_points)


class AugmentedHomographyModel(torch.nn.Module):
    def __init__(self, homography: TensorT, affine_matrices: TensorT, points_shape: torch.Size) -> None:
        super().__init__()
        
        assert homography.shape == (3, 3)
        assert (affine_matrices.ndim == 3) and (affine_matrices.shape[1:] == (3, 3))
        
        self.homography: torch.nn.Parameter = torch.nn.Parameter(homography)
        self.translation_coefs: torch.nn.Parameter = torch.nn.Parameter(torch.zeros(points_shape))
        self.affine_matrices: TensorT = affine_matrices
    
    def forward(self, points: TensorT) -> TensorT:
        # Points are a list of matrices.
        # Returns points in homogeneous coordinates.
        
        # First, apply perspective transformation and then affine in attempt to match the
        # rectified main points. Then broadcast the division by the z coordinate and convert each
        # column vector of [x, y, z] to [x', y', 1].
        return torch.stack(
            [x / x[-1, :] for x in torch.einsum(
                'gij,jk,gkl->gil',
                self.affine_matrices,
                self.homography,
                points + self.translation_coefs)])

# class AugmentedHomographyModel(torch.nn.Module):
#     def __init__(self, homography: TensorT, affine_matrices: TensorT) -> None:
#         super().__init__()
#
#         assert homography.shape == (3, 3)
#         assert (affine_matrices.ndim == 3) and (affine_matrices.shape[1:] == (3, 3))
#
#         self.homography: torch.nn.Parameter = torch.nn.Parameter(homography)
#         self.affine_matrices: TensorT = affine_matrices
#
#     def forward(self, points: TensorT) -> TensorT:
#         # Points are a list of matrices.
#         # Returns points in homogeneous coordinates.
#
#         # First, apply perspective transformation and then affine in attempt to match the
#         # rectified main points. Then broadcast the division by the z coordinate and convert each
#         # column vector of [x, y, z] to [x', y', 1].
#         return torch.stack(
#             [x / x[-1, :] for x in torch.einsum(
#                 'gij,jk,gkl->gil',
#                 self.affine_matrices,
#                 self.homography,
#                 points)])


class AugmentedHomographyTrainer:
    
    def __init__(self, src_points_groups: np.ndarray, dst_points_groups: np.ndarray) -> None:
        """Builds a trainer for the  perspective transformer using the presence of multiple groups
        of source points to refine the homography transformation using nonlinear optimization.

        :param src_points_groups: a list of groups of source points belonging to different objects
        in the plane while sharing the same dimensional properties
        :param dst_points_groups: a list of groups of destination points to which all the source
        point groups have to be mapped in the same fashion. However, only the first group will be
        used as the main one, therefore if all the groups contain the same points nothing
        unexpected will happen.
        """
        assert src_points_groups.ndim == 3
        assert src_points_groups.shape == dst_points_groups.shape
        
        self._src_points_groups: np.ndarray = src_points_groups
        self._dst_points_groups: np.ndarray = dst_points_groups
    
    def train(
            self,
            top_k_points_groups: Optional[int] = None,
            epoch_cb: Optional[EpochCbT] = None) -> OptimizationResult:
        """Trains the augmented homography model. Returns the history of loss function for each
        epoch.
        
        :param top_k_points_groups: number of top-k points groups to consider for the optimization
        (the assumption is that some points groups are noisy and should be avoided).
        :param epoch_cb: an optional callback to be called after each epoch. If the function returns
        false the training stops, otherwise it should return true.
        :return: the best homography matrix that could be found together with the history of the
        loss function evaluation after each epoch
        """
        assert (not top_k_points_groups) or (top_k_points_groups > 0)
        
        points_groups_stats = self._eval_points_groups_fitness()
        points_groups_stats.sort(key=lambda x: x.loss)
        
        _log.info('\n' + '\n'.join(
            f'index={s.ref_points_group_index} | loss={s.loss:.6f} | det={s.homography_det:.6f}'
            for s in points_groups_stats))
        
        if top_k_points_groups is not None:
            top_k_points_groups = min(len(points_groups_stats), top_k_points_groups)
            points_groups_stats = points_groups_stats[:top_k_points_groups]
        
        # The best transformation seeds. It is essentially the points group that has a
        # positive determinant while having the loss score minimal. It should be the first one.
        if points_groups_stats[0].homography_det <= 1e-4:
            raise TransformationNotFoundError('the best homography is a degenerate case')
        
        used_point_groups_indices = np.array(
            [s.ref_points_group_index for s in points_groups_stats])
        
        return self._optimize_homography(
            _OptimizationConfig(epoch_cb=epoch_cb),
            used_point_groups_indices)
    
    def _eval_points_groups_fitness(self) -> List[_PointsGroupEvalStats]:
        return list(map(self._eval_points_group_as_ref_group, range(len(self._src_points_groups))))
    
    def _eval_points_group_as_ref_group(
            self,
            ref_points_group_index:
            int,
            determinant_weight: float = 0.5) -> _PointsGroupEvalStats:
        ref_points = self._src_points_groups[ref_points_group_index]
        dst_points = self._dst_points_groups[ref_points_group_index]
        
        homography, _ = cv.findHomography(ref_points, dst_points)
        affine_matrices = self._optimize_affine_matrices(
            homography,
            self._src_points_groups,
            self._dst_points_groups,
            ref_points_group_index)
        homography_tensor = _T(homography)

        src_points_tensor = _convert_to_homogeneous(self._src_points_groups)
        dst_points_tensor = _convert_to_homogeneous(self._dst_points_groups)
        
        model = AugmentedHomographyModel(homography_tensor, _T(affine_matrices), src_points_tensor.shape)
        with torch.no_grad():
            outputs = model(src_points_tensor)
            loss = l2_norm_se_determinant_loss(
                outputs,
                dst_points_tensor[ref_points_group_index],
                homography_tensor,
                determinant_weight=determinant_weight)
        
        return _PointsGroupEvalStats(
            ref_points_group_index,
            loss.item(),
            homography_tensor,
            np.linalg.det(homography))
    
    @staticmethod
    def _optimize_affine_matrices(
            homography: np.ndarray,
            src_points_groups: np.ndarray,
            dst_points_groups: np.ndarray,
            ref_points_group_index: int = 0) -> np.ndarray:
        dst_points = dst_points_groups[ref_points_group_index]
        affine_matrices = np.empty((len(src_points_groups), 3, 3))
        
        for i, src_points_group in enumerate(src_points_groups):
            if i == ref_points_group_index:
                # In case of the main points group, no affine transformation is needed, so we
                # just use the identity mapping for clarity
                # | 1 0 0 |
                # | 0 1 0 |
                # | 0 0 1 |
                affine_matrices[i, :] = np.eye(3)
            else:
                try:
                    src_points_group_t = _transform_points(src_points_group, homography)
                    # Estimates a 2x3 affine transformation matrix:
                    # | r11 r12 t1 |
                    # | r21 r22 t2 |
                    affine_matrix, _ = cv.estimateAffine2D(src_points_group_t, dst_points)
                    # Expand the 2x3 affine matrix to 3x3:
                    # | r11 r12 t1 |
                    # | r21 r22 t2 |
                    # | 0   0   1  |
                    affine_matrix = np.concatenate((affine_matrix, [(0, 0, 1)]))
                    affine_matrices[i, :] = affine_matrix
                except ValueError as e:
                    raise TransformationNotFoundError(f'affine transformation not found: {str(e)}')
        
        return affine_matrices
    
    @staticmethod
    def _estimate_homography_determinant(src_points: np.ndarray, dst_points: np.ndarray) -> float:
        assert src_points.shape == dst_points.shape
        
        # FIXME The average takes the length not the no. of combinations.
        return sum(map(lambda p: np.linalg.norm(dst_points[p[0]] - dst_points[p[1]]) /
                                 np.linalg.norm(src_points[p[0]] - src_points[p[1]]),
                       itertools.combinations(range(len(src_points)), 2))) / len(src_points)
    
    def _optimize_homography(
            self,
            config: _OptimizationConfig,
            used_points_groups_indices: np.ndarray) -> OptimizationResult:
        used_src_points_groups = self._src_points_groups[used_points_groups_indices]
        used_dst_points_groups = self._dst_points_groups[used_points_groups_indices]
        used_src_points_tensors = _convert_to_homogeneous(used_src_points_groups)
        used_dst_points_tensors = _convert_to_homogeneous(used_dst_points_groups)
        dst_points = used_dst_points_tensors[0]
        
        homography, _ = cv.findHomography(used_src_points_groups[0], used_dst_points_groups[0])
        affine_matrices = self._optimize_affine_matrices(
            homography,
            used_src_points_groups,
            used_dst_points_groups)
        model = AugmentedHomographyModel(_T(homography), _T(affine_matrices), used_src_points_tensors.shape)
        optimizer = torch.optim.LBFGS(
            model.parameters(),
            lr=config.learning_rate,
            max_iter=config.optimizer_max_iter,
            tolerance_grad=config.tolerance_grad,
            history_size=config.history_size)
        
        start_lr = config.learning_rate
        end_lr = config.learning_rate / 10
        gamma = (end_lr / start_lr) ** (1.0 / (config.max_global_iter * config.max_local_iter))
        scheduler = torch.optim.lr_scheduler.ExponentialLR(optimizer, gamma)
        
        target_determinant = max(1, self._estimate_homography_determinant(
            used_src_points_groups[0],
            used_dst_points_groups[0]))
        _log.info(f'estimated determinant={target_determinant:.6f}')
        
        loss_history = []
        global_min_loss = float('inf')
        best_homography = None
        start_time = time.time()
        
        model.train()
        for _ in range(config.max_global_iter):
            local_min_loss = global_min_loss
            
            for i in range(config.max_local_iter):
                loss = None
                
                def closure():
                    nonlocal loss
                    optimizer.zero_grad()
                    outputs = model(used_src_points_tensors)
                    loss = experimental_loss(
                        outputs,
                        dst_points,
                        model.homography,
                        model.translation_coefs,
                        target_determinant,
                        config.determinant_weight)
                    # loss = l2_norm_se_determinant_loss(
                    #     outputs,
                    #     dst_points,
                    #     model.homography,
                    #     target_determinant,
                    #     config.determinant_weight)
                    loss.backward()
                    
                    # Zero gradient for the scale factor to prevent optimization.
                    model.homography.grad[2, 2] = 0
                    # model.translation_coefs.grad[:, 2, :] = 0
                    
                    return loss
                
                optimizer.step(closure)
                scheduler.step()
                
                curr_loss = loss.item()
                
                if curr_loss < local_min_loss:
                    local_min_loss, best_homography = curr_loss, model.homography.detach().numpy()
                
                loss_history.append(curr_loss)
                if (config.epoch_cb is not None) and (not config.epoch_cb(i, curr_loss)):
                    break
                
                if (time.time() - start_time) > config.max_time_sec:
                    break
            
            global_min_loss = local_min_loss
            
            model.affine_matrices = _T(self._optimize_affine_matrices(
                best_homography,
                used_src_points_groups + np.transpose(model.translation_coefs.detach().numpy()[:,:2,:], axes=(0, 2, 1)),
                used_dst_points_groups))
        elapsed_time = time.time() - start_time
        
        _log.info(f'translation stats: mean={torch.mean(model.translation_coefs):.12f} std={torch.std(model.translation_coefs):.12f}')
        
        return OptimizationResult(
            used_points_groups_indices,
            best_homography,
            np.float32(loss_history),
            elapsed_time)
