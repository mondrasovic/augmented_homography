#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Milan Ondrasovic <milan.ondrasovic@gmail.com>

from typing import Optional, Tuple

import cv2 as cv
import numpy as np

from common.dtypes import ShapeT
from transform.aughomography import AugmentedHomographyTrainer, OptimizationResult
from transform.base import HomographyTransformer


class PerspectiveTransformer(HomographyTransformer):
    """A perspective transformer that applies a homography transformation."""
    
    def __init__(self, homography: np.ndarray) -> None:
        """Constructor.

        :param homography: a homography matrix performing the perspective transformation
        """
        self._homography = homography
    
    @property
    def homography(self) -> np.ndarray:
        """Inherit docs from :class:`HomographyTransformer`."""
        return self._homography
    
    @staticmethod
    def build_from_correspondences(src_points: np.ndarray, dst_points: np.ndarray) \
            -> 'PerspectiveTransformer':
        """Builds a homography transformation based upon multiple (at least 4) point correspondences
        between two planes.

        :param src_points: points in the source plane
        :param dst_points: points in the destination (target) plane
        :return: an instance of the :class:`PerspectiveTransformer`
        """
        return PerspectiveTransformer(cv.findHomography(src_points, dst_points)[0])
    
    def _build_homography(self, image_shape: ShapeT = None) -> np.ndarray:
        return self._homography


class AugmentedHomographyTransformer(HomographyTransformer):
    """A perspective transformer that applies a homography transformation. This class allows the
    construction of homography mapping using multiple correspondences of points belonging to
    multiple objects of equal dimensions in the source plane. The advantage is that the relative
    position of individual objects to each other in the plane does not have to be known. This
    approach is EXPERIMENTAL as part of the research.
    """
    
    def __init__(self, homography: np.ndarray) -> None:
        """Constructor.

        :param homography: a homography matrix performing the perspective transformation
        """
        self._homography = homography
    
    @property
    def homography(self) -> np.ndarray:
        """Inherit docs from :class:`HomographyTransformer`."""
        return self._homography
    
    @staticmethod
    def build_from_augmented_correspondences(
            src_points_groups: np.ndarray,
            dst_points_groups: np.ndarray,
            top_k_points_groups: Optional[int] = None)\
        -> Tuple['AugmentedHomographyTransformer', OptimizationResult]:
        """Builds a perspective transformer using the presence of multiple groups of source points
        to refine the homography transformation using nonlinear optimization.

        :param src_points_groups: a list of groups of source points belonging to different objects
        in the plane while sharing the same dimensional properties
        :param dst_points_groups: a list of groups of destination points to which all the source
        point groups have to be mapped in the same fashion. However, only one group will be used as
        the main one, therefore if all the group contain the same points nothing unexpected will
        happen.
        :param top_k_points_groups: number of top-k points groups to consider for the optimization
        (the assumption is that some points groups are noisy and should be avoided).
        :return:
        """
        
        def on_epoch_end(epoch: int, loss: float) -> bool:
            print(f'\repoch: {epoch + 1} | loss: {loss:.6f}', end='')
            return True
        
        homography_trainer = AugmentedHomographyTrainer(src_points_groups, dst_points_groups)
        optimization_result = homography_trainer.train(top_k_points_groups, on_epoch_end)
        print()  # TODO Remove this ugly trick.
        
        transformer = AugmentedHomographyTransformer(optimization_result.homography)
        return transformer, optimization_result
    
    def _build_homography(self, image_shape: ShapeT = None) -> np.ndarray:
        return self._homography
