#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Milan Ondrasovic <milan.ondrasovic@gmail.com>

import tqdm
import logging
import dataclasses
from pathlib import Path

from typing import Callable, Iterable, Iterator, Optional, Tuple, Sequence

import numpy as np

import config

from common.dtypes import ShapeT
from transform.base import HomographyTransformer
from transform.rankedhomography import TransformationNotFoundError
from transform.perspective import (
    RankedPerspectiveTransformer, PerspectiveTransformer)
from datagen.experiments import DataFileNames

_log = logging.getLogger(__name__)

HomographyBuilderT = Callable[[np.ndarray, np.ndarray], HomographyTransformer]

@dataclasses.dataclass()
class EvaluationParams:
    points_groups_orig: np.ndarray
    points_groups_warped: np.ndarray
    pix_coords_orig: np.ndarray
    pix_coords_warped: np.ndarray
    img_shape_orig: ShapeT
    output_dir: Optional[Path] = None


def np_load(dir_path: Path, file_name: str) -> np.ndarray:
    return np.load(str(dir_path / f'{file_name}.npy'))


def iter_dirs_only(dir_path: Path) -> Iterator[Path]:
    return filter(Path.is_dir, dir_path.iterdir())


def eval_rectification(
        transformer: HomographyTransformer, pix_coords_orig: np.ndarray,
        pix_coords_warped: np.ndarray,
        img_shape_orig: ShapeT) -> Tuple[np.ndarray, np.ndarray]:
    pix_coords_rectified = transformer.transform_points(
        pix_coords_warped, img_shape_orig)
    errors = np.linalg.norm(pix_coords_orig - pix_coords_rectified, axis=1)
    return errors.reshape(img_shape_orig), pix_coords_rectified


def opencv_homography_builder(
        points_orig: np.ndarray,
        points_warped: np.ndarray) -> HomographyTransformer:
    return PerspectiveTransformer.build_from_correspondences(
        points_warped, points_orig)


def ranked_homography_builder(
        points_groups_orig: np.ndarray,
        points_groups_warped: np.ndarray) ->\
        Tuple[RankedPerspectiveTransformer, Sequence[int]]:
    return RankedPerspectiveTransformer.build_from_augmented_correspondences(
        points_groups_warped, points_groups_orig)


def save_rectification_results(
        output_dir: Path, results_id: int, error_grid: np.ndarray,
        pix_coords_rectified: np.ndarray, transformer: HomographyTransformer,
        used_points_groups_id: Iterable[int]) -> None:
    curr_output_dir = output_dir / f'result_{results_id:04d}'
    curr_output_dir.mkdir(parents=True, exist_ok=True)
    
    def save(file_name: str, data: np.ndarray):
        np.save(str(curr_output_dir / f'{file_name}.npy'), data)
    
    save('error_grid', error_grid)
    save('pix_coords_rectified', pix_coords_rectified)
    save('used_points_groups_ids', np.array(used_points_groups_id))
    save('homography', transformer.homography)


def evaluate_ranked_homography(params: EvaluationParams) -> None:
    try:
        transformer, used_point_groups_indices =\
            ranked_homography_builder(
                params.points_groups_orig,
                params.points_groups_warped)
    except TransformationNotFoundError as e:
        _log.error(str(e))
    else:
        error_grid, pix_coords_rectified = eval_rectification(
            transformer, params.pix_coords_orig,
            params.pix_coords_warped, params.img_shape_orig)
        save_rectification_results(
            params.output_dir, 1, error_grid, pix_coords_rectified,
            transformer, used_point_groups_indices)


def evaluate_opencv_homography(params: EvaluationParams) -> None:
    for i, (vertices_original, vertices_transformed) in enumerate(
            zip(params.points_groups_orig,
                params.points_groups_warped)):
        transformer = opencv_homography_builder(
            vertices_original, vertices_transformed)
        error_grid, pix_coords_rectified = eval_rectification(
            transformer, params.pix_coords_orig,
            params.pix_coords_warped, params.img_shape_orig)
        save_rectification_results(
            params.output_dir, i + 1, error_grid, pix_coords_rectified,
            transformer, (i,))


def run_experiments(input_dir_path: str, output_dir_path: str) -> None:
    results_dir = Path(output_dir_path)
    
    experiment_dir = Path(input_dir_path)
    img_shape_orig = np_load(experiment_dir, DataFileNames.IMG_SHAPE_ORIG)
    pix_coords_orig = np_load(experiment_dir, DataFileNames.PIX_COORDS_ORIG)
    
    for inst_dir in iter_dirs_only(experiment_dir):
        _log.info(f'processing instance: {inst_dir.stem}')
        
        pix_coords_warped = np_load(inst_dir, DataFileNames.PIX_COORDS_WARPED)
        assert pix_coords_orig.shape == pix_coords_warped.shape
        
        points_groups_orig = np_load(inst_dir, DataFileNames.POINTS_GROUPS_ORIG)
        points_groups_warped = np_load(
            inst_dir, DataFileNames.POINTS_GROUPS_WARPED)
        params = EvaluationParams(
            points_groups_orig, points_groups_warped, pix_coords_orig,
            pix_coords_warped, img_shape_orig)
        
        curr_results_dir = results_dir / inst_dir.relative_to(experiment_dir)
        
        _log.info(f'generating opencv results')
        params.output_dir = curr_results_dir / 'opencv_results'
        evaluate_opencv_homography(params)
        
        _log.info(f'generating ranked homography results')
        params.output_dir = curr_results_dir / 'ranked_homography_results'
        evaluate_ranked_homography(params)
        _log.info('=' * 80)


def main() -> int:
    # logging.basicConfig(
    #     filename='experiments_log.log',
    #     format='[%(asctime)s][%(name)s][%(levelname)s]: %(message)s',
    #     level=logging.INFO)
    logging.basicConfig(
        level=logging.INFO,
        format='[%(asctime)s][%(name)s][%(levelname)s]: %(message)s',
        handlers=[
            logging.FileHandler('../experiments_log.log'),
            #logging.StreamHandler(sys.stdout)
        ])

    total_iters = len(config.EXPERIMENT_TYPES) * len(config.N_GROUPS_VALS)
    with tqdm.tqdm(total=total_iters) as pbar:
        for experiment in config.EXPERIMENT_TYPES:
            for n_groups in config.N_GROUPS_VALS:
                dir_name = experiment.build_dir_name(n_groups)
                input_dir_path = config.DATA_DIR_PATH / dir_name
                output_dir_path = config.RESULTS_DIR_PATH / dir_name
                
                run_experiments(str(input_dir_path), str(output_dir_path))
                pbar.update()
    
    return 0


if __name__ == '__main__':
    import sys
    
    sys.exit(main())
