#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Milan Ondrasovic <milan.ondrasovic@gmail.com>

import click
import cv2 as cv
import numpy as np

from transform.perspective import AugmentedHomographyTransformer, PerspectiveTransformer


def read_points_from_file(points_file_path: str, shape_vertices_num: int) -> np.ndarray:
    points = []
    
    with open(points_file_path, 'r') as in_file:
        for row, col in map(str.split, filter(lambda l: len(l) > 0, in_file.readlines())):
            points.append((float(row.strip()), float(col.strip())))
    
    assert (len(points) % shape_vertices_num) == 0
    
    return np.float32(points).reshape((len(points) // shape_vertices_num, shape_vertices_num, 2))


@click.command()
@click.argument('src_points_file_path')
@click.argument('dst_points_file_path')
@click.argument('homography_file_path')
@click.argument('shape_vertices_num')
@click.argument('method')
def main(
        src_points_file_path: str,
        dst_points_file_path: str,
        homography_file_path: str,
        shape_vertices_num: str,
        method: str) -> int:
    src_points = read_points_from_file(src_points_file_path, int(shape_vertices_num))
    dst_points = read_points_from_file(dst_points_file_path, int(shape_vertices_num))
    
    if method == 'standard':
        transformer = PerspectiveTransformer.build_from_correspondences(
            src_points[0],
            dst_points[0]
        )
    elif method == 'augmented':
        transformer, _ = \
            AugmentedHomographyTransformer.build_from_augmented_correspondences(
                src_points,
                dst_points
            )
    else:
        raise ValueError(f'unrecognized method name {method}')
    
    # TODO Make this parametric.
    image = cv.imread('../test_warped_image.png', cv.IMREAD_COLOR)
    image_transformed = transformer.transform_image(image, output_shape=image.shape)
    cv.imwrite('../test_warped_image_rectified.png', image_transformed)
    
    np.save(homography_file_path, transformer.homography)
    
    return 0


if __name__ == '__main__':
    import sys
    
    
    sys.exit(main())
