#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Milan Ondrasovic <milan.ondrasovic@gmail.com>

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from typing import Iterable

from dataproc import merge_mean_errors_for_groups

def plot_top_k_markers_error_dependency(
        errors_df: pd.DataFrame,
        n_groups_values: Iterable[int],
        output_file_path: str) -> None:
    error_values = merge_mean_errors_for_groups(errors_df, n_groups_values)
    errors_n_groups_df = pd.DataFrame(
        error_values, columns=list(map(str, n_groups_values)),
        index=list(range(len(error_values))))
    
    errors_n_groups_df.boxplot(
        grid=False, showfliers=False, patch_artist=True, notch=True)
    
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)

    plt.title('Top-k Markers vs. Re-projection Error')
    plt.xlabel('No. of used top-k markers (out of 9)')
    plt.ylabel('Mean $L_2$-norm re-projection error')
    
    plt.tight_layout()
    plt.savefig(output_file_path, dpi=300)

def build_aug_homography_vs_best_marker_table(errors_df: pd.DataFrame) -> pd.DataFrame:
    opencv_results = errors_df[errors_df['method'] == 'opencv']
    n_groups = errors_df['n_groups'].max()
    selection = [False] * opencv_results.shape[0]
    marker_candidates = errors_df[
        (errors_df['method'] == 'augmented_homography') & (errors_df['n_groups'] == 1)]
    for i, c in enumerate(marker_candidates['groups']):
        selection[i * n_groups + c[0]] = True

    relevant_cols = ['layout', 'rotation', 'noise', 'min', 'max', 'mean', 'median', 'std_dev']
    groupby_cols = ['layout', 'rotation', 'noise']
    aug_homography_results = errors_df[errors_df['method'] == 'augmented_homography']
    
    aug_homography_best_results = aug_homography_results[relevant_cols].groupby(
        groupby_cols).mean()
    opencv_best_marker_results = opencv_results[selection][relevant_cols].groupby(
        groupby_cols).min()
    
    improvement_values = {'min': [], 'max': [], 'std_dev': [], 'mean': [], 'median': []}
    for col in improvement_values.keys():
        improvement_aug_vs_best_marker = (
                (opencv_best_marker_results[col] - aug_homography_best_results[col]) /
                opencv_best_marker_results[col]).values
        improvement_values[col].append(np.mean(improvement_aug_vs_best_marker))
    
    return pd.DataFrame(improvement_values)
